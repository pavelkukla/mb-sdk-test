const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

const base = require('./webpack.base.js');

module.exports = (webpackEnv = {}) => {
  webpackEnv.development = true;

  const disableHotReload = process.env.devServerDisableHMR === 'true';
  const isHttps = process.env.devServerHttps === 'true';

  let port = process.env.devServerPort || 9900;
  if (webpackEnv.sandbox) {
    port = process.env.devServerSandboxPort || port;
  }

  return merge(base(webpackEnv), {
    mode: 'development',
    devtool: process.env.devtool || 'eval',
    cache: true,
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      compress: true,
      port,
      https: isHttps,
      host: process.env.devServerHost || 'localhost',
      disableHostCheck: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      hot: !disableHotReload,
      inline: !disableHotReload,
      watchOptions: process.env.devServerWatchOptionsDisable ? {} : {
        aggregateTimeout: process.env.devServerWatchAggregate || 30,
        poll: process.env.devServerWatchPoll || 1000,
      },
    },
    plugins: [
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),

    ],
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'src', 'modules', 'Legacy', 'Common', 'es6'),
          ],
        }, {
          test: /\.(pug)$/,
          loader: 'pug-lint-loader',
          exclude: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'src', 'modules', 'Legacy'),
          ],
          options: Object.assign(
            { emitError: true },
            require('./.pug-lintrc.js'), // eslint-disable-line global-require
          ),
        },
      ],
    },
  });
};
