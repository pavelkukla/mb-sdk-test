// Error levels
const WARN = 'warn';
const ERROR = 'error';
const OFF = 'off';

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      legacyDecorators: true
    },
  },
  env: {
    browser: true,
  },
  extends: [
    'airbnb-base',
    'plugin:jest/recommended',
    'plugin:vue/recommended',
  ],
  plugins: [
    'vue',
  ],
  // Check if imports are resolved correctly
  // considering aliases defined in webpack's config
  settings: {
    'import/resolver': {
      'webpack': {
        'config': 'webpack.base.js'
      }
    }
  },
  // add your custom rules here
  rules: {
    // Ensure that there is no resolvable path back to this module via its dependencies
    'import/no-cycle': [
      ERROR, {
        maxDepth: 1,
      }
    ],
    // Forbid importing modules that are not declared in package.json's dependencies
    'import/no-extraneous-dependencies': [
      ERROR, {
        // Allow importing devDependencies in tests and node.js scripts
        devDependencies: [
          "/*.js",
          "**/*.spec.js",
          "/tools/**/*.js"
        ]
      }
    ],
    // Don't require extension when importing
    'import/extensions': [ERROR, 'never'],
    // Allow exporting single named function/class
    'import/prefer-default-export': OFF,
    // Allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // Max characters allowed in a single line of code
    'max-len': [ERROR, { 'code': 120 }],
    // Skip checking empty lines after single-line class members
    'lines-between-class-members': [
      ERROR, 'always', {
        'exceptAfterSingleLine': true
      }
    ],
    // Allow placing object props in both single line and multiple lines
    // Allow creating class methods that don't use `this`
    'class-methods-use-this': OFF,
    // Indent content of script tag in .vue files
    'vue/script-indent': [
      ERROR, 2, {
        'baseIndent': 1,
        'switchCase': 1,
    }],
    'no-useless-return': OFF,
    'prefer-destructuring': OFF,
    'no-param-reassign': OFF,
    'no-mixed-operators': OFF,
    'object-curly-newline': [ERROR, {
      minProperties: 5,
      consistent: true,
    }],
    'no-warning-comments': [WARN, {
      terms: ['debug', 'deleteme', 'fixme', 'removeme', 'temp', 'uncommentme', 'xxx'],
    }],
  },
  overrides: [
    {
      // When default rules are turned on eslint lints both js and vue files
      // rules that have a corresponding vue-specific rules defined must be disabled
      files: ['*.vue'],
      rules: {
        'indent': OFF
      }
    },
    {
      files: [
        "**/__tests__/*.{js,ts}?(x)",
        "**/*.spec.{js,ts}",
      ],
      env: {
        jest: true,
      },
      plugins: [
        'jest',
      ],
    }
  ],
};
