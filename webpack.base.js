const path = require('path');
const { each, trimEnd } = require('lodash');
const { VueLoaderPlugin } = require('vue-loader');
const jsConfig = require('./jsconfig.json');

const alias = {
  vue$: 'vue/dist/vue.esm.js',
};

each(jsConfig.compilerOptions.paths, (destination, pathAlias) => {
  const webpackAlias = trimEnd(pathAlias, '/*');
  const webpackPath = trimEnd(destination[0], '/*');
  alias[webpackAlias] = path.resolve(__dirname, webpackPath);
});

module.exports = (webpackEnv = {}) => ({
  entry: [
    '@babel/polyfill',
    './src',
  ],
  output: {
    filename: 'mb-app.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    alias,
    extensions: ['*', '.js', '.ts', '.vue', '.json', '.pug'],
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'cache-loader',
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { sourceMap: !webpackEnv.production },
          },
        ],
      }, {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader',
        ],
      }, {
        test: /(public_accessa|src[\\/]modules[\\/]Legacy[\\/]).*\.js$/,
        use: [
          'script-loader',
        ],
      },
      {
        test: /\.hbs$/,
        use: [
          'cache-loader',
          {
            loader: 'html-loader',
            options: {
              attrs: false,
              ignoreCustomFragments: [
                /{{.*?}}/,
                /{{{.*?}}}/,
              ],
              minimize: true,
            },
          },
        ],
      }, {
        test: /\.(js|babel)$/,
        include: [
          path.resolve(__dirname, '.sandbox'),
          path.resolve(__dirname, 'src'),
          path.resolve(__dirname, 'node_modules'),
        ],
        exclude: (file) => {
          file = file.replace(/\\/g, '/');
          return (/node_modules/.test(file) && !/\.vue(\.babel|\.js)$/.test(file))
          || RegExp('src/modules/Legacy/(?!Common/es6)').test(file);
        },
        use: [
          'cache-loader',
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
            },
          },
        ],
      }, {
        test: /\.pug$/,
        oneOf: [
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader'],
          }, {
            use: ['raw-loader', 'pug-plain-loader'],
          },
        ],
      }, {
        test: /\.(styl|stylus)$/,
        use: [
          'cache-loader',
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { sourceMap: !webpackEnv.production },
          },
          'stylus-loader',
        ],
      }, {
        test: /\.tsx?$/,
        use: [
          'cache-loader',
          {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/],
            },
          },
          'tslint-loader',
        ],
      }, {
        test: /\.vue$/,
        use: [
          'cache-loader',
          'vue-loader',
          'eslint-loader',
        ],
      }, {
        test: /\.vue$/,
        exclude: /node_modules/,
        enforce: 'pre',
        use: [
          {
            loader: 'vue-tslint-loader',
            options: {
              emitErrors: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin(),
  ]
});
