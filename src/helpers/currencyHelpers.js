export const FORMATS = {
  country: 'United States',
  country_code: 'us',
  currency_code: 'USD',
  currency_name: 'US Dollar',
  currency_symbol: 'USD',
  currency_position: 'before',
  current_timezone_offset: 0,
  date_format: 'm/d/Y',
  date_format_full: 'l, F j, Y',
  date_format_short: 'm/d',
  date_format_wy: 'm/d',
  date_format_moment_js: 'MM/DD/YYYY',
  default_language: 'en',
  default_timezone: 'UTC',
  locale: 'en_US',
  mon_decimal_point: ',',
  mon_thousands_sep: ' ',
  month_first: true,
  name: 'english',
  native_short: 'En',
  phone: '',
  time_format: 'h:i A',
};

export const getCurrencyFormats = (isHotelCurrency) => {
  if (isHotelCurrency) {
    return {
      currency_code: FORMATS.currency_code,
      currency_name: FORMATS.currency_name,
      currency_symbol: FORMATS.currency_symbol,
      currency_position: FORMATS.currency_position,
    };
  }

  return Object.assign({}, {});
};

export const getJsRounding = (val) => {
  const str = String(val);
  let jsRoundLen = 3;
  const dotPosition = str.indexOf('.');

  if (dotPosition !== -1) {
    jsRoundLen = str.length - dotPosition;

    if (jsRoundLen <= 2) {
      jsRoundLen = 3;
    }
  }

  return 1 / (10 ** jsRoundLen);
};

export const formatCurrency = (str, isHotelCurrency) => {
  let tstr = String(str);
  const currencyFormats = getCurrencyFormats(isHotelCurrency);

  if (tstr.indexOf(currencyFormats.currency_symbol) > -1) {
    return str;
  }

  tstr = parseFloat(tstr) + getJsRounding(tstr);
  tstr = String(tstr.toFixed(2));
  tstr = tstr.replace('.', FORMATS.mon_decimal_point);

  let negative = false;
  if (tstr.indexOf('-') === 0) {
    negative = true;
    tstr = tstr.replace('-', '');
  }

  if (tstr.length > 6) {
    tstr = tstr.split(FORMATS.mon_decimal_point);
    const begin = tstr[0].length - (parseInt(tstr[0].length / 3, 10) * 3);
    let newStr = begin === 0 ? '' : tstr[0].substr(0, begin) + FORMATS.mon_thousands_sep;
    for (let i = begin; i < tstr[0].length; i += 3) {
      newStr += tstr[0].substr(i, 3) + FORMATS.mon_thousands_sep;
    }
    tstr = (FORMATS.mon_thousands_sep
      ? newStr.substr(0, newStr.length - 1)
      : newStr) + FORMATS.mon_decimal_point + tstr[1];
  }
  let string = '{currency_symbol} {float}';
  if (currencyFormats.currency_position === 'after') {
    string = '{float} {currency_symbol}';
  }

  return string
    .replace('{currency_symbol}', currencyFormats.currency_symbol)
    .replace('{float}', (negative ? '-' : '') + tstr)
    .trim();
};
