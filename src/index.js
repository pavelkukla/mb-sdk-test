import Vue from 'vue';
import vueCustomElement from 'vue-custom-element';
import App from './components/App';
import 'bootstrap/dist/css/bootstrap.min.css';


Vue.use(vueCustomElement);
Vue.customElement('cb-app', App);
