const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const base = require('./webpack.base.js');
module.exports = (webpackEnv = {}) => {
  webpackEnv.production = true;

  return merge(base(webpackEnv), {
    mode: 'production',
    devtool: 'hidden-source-map',
    plugins: [
      new CleanWebpackPlugin(),
      new CompressionPlugin({
        test: /\.js/,
        filename: '[path].gz[query]',
        deleteOriginalAssets: true,
      }),
      new webpack.SourceMapDevToolPlugin({
        filename: '[file].sourcemap',
        append: '\n//# sourceMappingURL=app.js.sourcemap.gz',
      }),
    ],
  });
};
